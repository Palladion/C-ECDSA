#ifndef ECC_H
#define ECC_H

#include <gmp.h>
#include <stdio.h>

struct point {
    mpz_t x;
    mpz_t y;
};

struct curve {
    mpz_t p; mpz_t a; mpz_t b; mpz_t n; struct point g;
};

struct key {
    mpz_t r;
    struct point p;
};

struct digest {
    mpz_t num;
    char* hex;
    unsigned char* digest;
};

struct num_return {
    mpz_t out;
};

void init_point(struct point *P);
void clear_point(struct point *P);
void init_curve(struct curve *P);
void init_digest(struct digest *P);
void clear_curve(struct curve *P);
void init_key(struct key *P);
void clear_key(struct key *P);
void clear_digest(struct digest *D);
void copy_point(struct point *out, struct point in);
int is_on_curve(struct point p, struct curve c);
void point_neg(struct point out, struct point p, struct curve c);
void point_add(struct point *out, struct point one, struct point two, struct curve c);
void scalar_mult(struct point * out, mpz_t k, struct point p, struct curve c);
void sign_message(struct point *out, mpz_t z, mpz_t priv, struct curve c);
void make_key(struct key *out, struct curve c);
void sha_point(struct digest *out, struct point p);
int verify_signature(struct point pub, mpz_t z, struct point sign, struct curve c);
char * compress(struct point p);
void decompress(struct point * out, char * in, struct curve c);
struct curve get_secp256k1(); struct curve get_p256(); struct curve get_p224();
struct curve get_p192(); struct curve get_p384(); struct curve get_p521();
struct curve get_secp112r1(); struct curve get_secp112r2();
#endif
