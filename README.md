# C-ECDSA

A ECDSA and ECC implementation based on Andrea Corbellinis python script for ECDSA

This project attempts to be a simple C library for ECDSA and ECC related projects

## Notice

This project is not audited and was made for educational purposes,
Its not recommended to use this project for important applications

## Credits

[The orginal python script this code is based on](https://github.com/andreacorbellini/ecc/blob/master/scripts/ecdsa.py)
