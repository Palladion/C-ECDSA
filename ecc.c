#include "ecc.h"
#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <openssl/sha.h>

void init_point(struct point *P) {
    mpz_init(P->x); mpz_init(P->y);
}

void clear_point(struct point *P) {
    mpz_clear(P->x); mpz_init(P->y);
}

void init_curve(struct curve *P) {
    mpz_init(P->p); mpz_init(P->a); mpz_init(P->b);
    mpz_init(P->n); init_point(&P->g);
}

void init_key(struct key *P) {
    mpz_init(P->r); init_point(&P->p);
}

void init_digest(struct digest *P) {
    mpz_init(P->num); P->hex = malloc(65*sizeof(char)); P->digest = malloc(SHA256_DIGEST_LENGTH*sizeof(unsigned char));
}

void clear_curve(struct curve *P) {
    mpz_clear(P->p); mpz_clear(P->a); mpz_clear(P->b);
    mpz_clear(P->n); clear_point(&P->g);
}

void clear_key(struct key *K) {
    mpz_clear(K->r); clear_point(&K->p);
}

void clear_digest(struct digest *D) {
    mpz_clear(D->num); free(D->hex); free(D->digest);
}

void copy_point(struct point *out, struct point in) {
    mpz_set(out->x,in.x); mpz_set(out->y,in.y);
}

// (y * y - x ** 3 - a * x - b) % p
// (R - test - L - b) % p
// (R - test - L) % p
// (test) % p

int is_on_curve(struct point p, struct curve c) {
    mpz_t test,M,R,L; mpz_init(test); mpz_init(R); mpz_init(L); mpz_init(M);
    mpz_pow_ui(M,p.x,3); mpz_mul(R,p.y,p.y); mpz_mul(L,c.a,p.x);
    mpz_sub(test,R,M); mpz_sub(test,test,L); mpz_sub(test,test,c.b);
    mpz_mod(test,test,c.p);
    int re = (mpz_cmp_ui(test,0) == 0);
    mpz_clear(test); mpz_clear(M); mpz_clear(R); mpz_clear(L); return re;
}

void point_neg(struct point out, struct point p, struct curve c) {
    mpz_set(out.x,p.x); mpz_neg(out.y,p.y); mpz_mod(out.y,out.y,c.p);
}

void point_add(struct point *out, struct point one, struct point two, struct curve c) {
    if (mpz_cmp_ui(one.x,69) == 0) {
        mpz_set(out->x,two.x); mpz_set(out->y,two.y); return;
    }
    int check = mpz_cmp(one.x,two.x); mpz_t m; mpz_t m2; mpz_init(m); mpz_init(m2);
    struct point Pout; init_point(&Pout);
    if (check == 0) {
        mpz_mul_ui(m,one.x,3); mpz_mul(m,m,one.x); mpz_add(m,m,c.a);
        mpz_mul_ui(m2,one.y,2); mpz_invert(m2,m2,c.p);
        mpz_mul(m,m,m2);
    } else {
        mpz_sub(m,one.y,two.y);
        mpz_sub(m2,one.x,two.x); mpz_invert(m2,m2,c.p);
        mpz_mul(m,m,m2);
    }
    mpz_mul(Pout.x,m,m); mpz_sub(Pout.x,Pout.x,one.x); mpz_sub(Pout.x,Pout.x,two.x);
    mpz_sub(Pout.y,Pout.x,one.x); mpz_mul(Pout.y,Pout.y,m); mpz_add(Pout.y,one.y,Pout.y);
    mpz_mod(Pout.x,Pout.x,c.p); mpz_neg(Pout.y,Pout.y); mpz_mod(Pout.y,Pout.y,c.p);
    mpz_clear(m); mpz_clear(m2); mpz_set(out->x,Pout.x); mpz_set(out->y,Pout.y);
    clear_point(&Pout);
}

void scalar_mult(struct point * out, mpz_t k, struct point p, struct curve c) {
    struct point Pout; init_point(&Pout);
    mpz_set_ui(Pout.x,69); mpz_set_ui(Pout.y,69);
    struct point addend,add2; init_point(&addend); mpz_set(addend.x,p.x); mpz_set(addend.y,p.y); init_point(&add2);
    mpz_t private; mpz_init(private); mpz_set(private,k); mpz_t check,one; mpz_init(check); mpz_init(one);
    while (mpz_cmp_ui(k,0) != 0) {
        mpz_set_ui(one,1); mpz_and(check,k,one);
        if (mpz_cmp_ui(check,1) == 0) {
            point_add(&Pout,Pout,addend,c);
        }
        point_add(&addend,addend,addend,c);
        mpz_fdiv_q_2exp(k,k,1);
    }
    mpz_set(k,private);
    mpz_set(out->x,Pout.x); mpz_set(out->y,Pout.y);
    clear_point(&Pout); clear_point(&addend); clear_point(&add2); mpz_clear(check); mpz_clear(one); mpz_clear(private);
}

void sign_message(struct point *out, mpz_t z, mpz_t priv, struct curve c) {
    mpz_t r,s,k,s2; mpz_init(r); mpz_init(s); mpz_init(k); mpz_init(s2);
    unsigned char * randval = malloc(34); FILE *f; f = fopen("/dev/random","r"); fread(randval,1,32,f); fclose(f); 
    mpz_import(k,32,1,1,0,0,randval); free(randval);
    struct point K; init_point(&K); scalar_mult(&K,k,c.g,c);
    mpz_mod(r,K.x,c.n); mpz_mul(s,r,priv); mpz_add(s,z,s);
    mpz_invert(s2,k,c.n); mpz_mul(s,s,s2); mpz_mod(s,s,c.n);
    mpz_set(out->x,r); mpz_set(out->y,s);
    mpz_clear(r); mpz_clear(s); mpz_clear(k); mpz_clear(s2); clear_point(&K);
}

int verify_signature(struct point pub, mpz_t z, struct point sign, struct curve c) {
    int out = 0;
    mpz_t w,u1,u2,r,s; mpz_init(r); mpz_init(s); mpz_set(r,sign.x); mpz_set(s,sign.y);
    mpz_init(w); mpz_invert(w,s,c.n);
    mpz_init(u1); mpz_mul(u1,z,w); mpz_mod(u1,u1,c.n);
    mpz_init(u2); mpz_mul(u2,r,w); mpz_mod(u2,u2,c.n);
    struct point L,R,o; init_point(&L); init_point(&R); init_point(&o);
    scalar_mult(&L,u1,c.g,c); scalar_mult(&R,u2,pub,c); point_add(&o,L,R,c);
    mpz_mod(r,r,c.n); mpz_mod(o.x,o.x,c.n);
    if (mpz_cmp(r,o.x) == 0) {
        out = 1;
    }
    mpz_clear(r); mpz_clear(s); clear_point(&o); mpz_clear(w);
    mpz_clear(u1); mpz_clear(u2); clear_point(&L); clear_point(&R);
    return out;
}

void make_key(struct key *out, struct curve c) {
    mpz_t k; mpz_init(k);
    //struct key o; init_key(&o);
    unsigned char * randval = malloc(34); FILE *f; f = fopen("/dev/random","r"); fread(randval,1,32,f); fclose(f);
    mpz_import(k,32,1,1,0,0,randval); free(randval);
    scalar_mult(&out->p,k,c.g,c); mpz_set(out->r,k);
}

void sha_point(struct digest *out, struct point p) {
    mpz_t x; mpz_init(x); mpz_set(x,p.x);
    char hex[70]; unsigned char hash[SHA256_DIGEST_LENGTH]; char text2[100]; gmp_sprintf(text2,"%Zx",x);
    SHA256_CTX hashContext; SHA256_Init(&hashContext); SHA256_Update(&hashContext,text2,strlen(text2)); SHA256_Final(hash,&hashContext);
    for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) { sprintf(&hex[x*2],"%02x",(unsigned int)hash[x]);  }
    mpz_t digest; mpz_init(digest); mpz_set_str(digest,hex,16);
    for (int x = 0; x < 64; x++) { out->hex[x] = hex[x]; }
    mpz_set(out->num,digest); for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) { out->digest[x] = hash[x]; }
    mpz_clear(x); mpz_clear(digest);

}

//only for secp256k1 for now
char * compress(struct point p) {
    char * out;
    char * hex = malloc(70); memset(hex,0,70);
    gmp_sprintf(hex,"%Zx",p.x); int size = strlen(hex); out = malloc(70); memset(out,'0',64);
    mpz_t check; mpz_init(check); mpz_mod_ui(check,p.y,2);
    char append;
    if (mpz_cmp_ui(check,0) == 0) {
        append = '2';
    } else {
        append = '3';
    }
    out[0] = '0'; out[1] = append; 
    //for (int x = 2; x < size+2; x++) { out[x] = hex[x-2]; }
    int a = 65; int b = size-1;
    for (int x = 0; x < size; x++) { out[a] = hex[b]; a--; b--;}
    mpz_clear(check); free(hex);
    return out;
}

void decompress(struct point * out, char * in, struct curve c) {
    int size = strlen(in);
    char * hex = malloc(size); memset(hex,0,size);
    for (int x = 2; x < size; x++) { hex[x-2] = in[x]; }
    mpz_set_str(out->x,hex,16); mpz_t a; mpz_init(a);
    mpz_pow_ui(a,out->x,3); mpz_add(a,a,c.b);
    mpz_t y; mpz_init(y); mpz_t h; mpz_init(h);
    mpz_add_ui(h,c.p,1); mpz_fdiv_q_ui(h,h,4);
    mpz_powm(y,a,h,c.p); mpz_t check; mpz_init(check);
    mpz_mod_ui(check,y,2); int odd;
    if (mpz_cmp_ui(check,0) == 0) {
        odd = 0;
    } else {
        odd = 1;
    }
    if (odd) {
        if (in[1] == '2') {
            mpz_neg(y,y); mpz_mod(y,y,c.p);
        }
    } else {
        if (in[1] == '3') {
            mpz_neg(y,y); mpz_mod(y,y,c.p);
        }
    }
    mpz_set(out->y,y);
    mpz_clear(a); mpz_clear(y); mpz_clear(h); mpz_clear(check);
}
