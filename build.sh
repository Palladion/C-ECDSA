cc -Wall -c ecc.c curve.c ecc.h
ar -cvq ecc.a ecc.o curve.o
cp ecc.a /usr/local/lib/
cp ecc.h /usr/include/ecc.h
rm -r ecc.a ecc.o curve.o ecc.h.gch
