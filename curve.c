#include "ecc.h"

struct curve get_secp112r1() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "db7c2abf62e35e668076bead208b";
    char astr[1024] = "db7c2abf62e35e668076bead2088";
    char bstr[1024] = "659ef8ba043916eede8911702b22";
    char gxstr[1024] = "09487239995a5ee76b55f9c2f098";
    char gystr[1024] = "a89ce5af8724c0a23e0e0ff77500";
    char strn[1024] = "db7c2abf62e35e7628dfac6561c5";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}

struct curve get_secp112r2() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "db7c2abf62e35e668076bead208b";
    char astr[1024] = "6127c24c05f38a0aaaf65c0ef02c";
    char bstr[1024] = "51def1815db5ed74fcc34c85d709";
    char gxstr[1024] = "4ba30ab5e892b4e1649dd0928643";
    char gystr[1024] = "adcd46f5882e3747def36e956e97";
    char strn[1024] = "36df0aafd8b8d7597ca10520d04b";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;

}

struct curve get_secp256k1() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "115792089237316195423570985008687907853269984665640564039457584007908834671663";
    char astr[1024] = "0";
    char bstr[1024] = "7";
    char gxstr[1024] = "55066263022277343669578718895168534326250603453777594175500187360389116729240";
    char gystr[1024] = "32670510020758816978083085130507043184471273380659243275938904335757337482424";
    char strn[1024] = "115792089237316195423570985008687907852837564279074904382605163141518161494337";
    mpz_set_str(c.p,pstr,10); mpz_set_str(c.a,astr,10); mpz_set_str(c.b,bstr,10); mpz_set_str(c.g.x,gxstr,10); mpz_set_str(c.g.y,gystr,10); mpz_set_str(c.n,strn,10);
    return c;
}

struct curve get_p256() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "ffffffff00000001000000000000000000000000ffffffffffffffffffffffff";
    char astr[1024] = "ffffffff00000001000000000000000000000000fffffffffffffffffffffffc";
    char bstr[1024] = "5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b";
    char gxstr[1024] = "6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296";
    char gystr[1024] = "4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5";
    char strn[1024] = "ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}

struct curve get_p224() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "ffffffffffffffffffffffffffffffff000000000000000000000001";
    char astr[1024] = "fffffffffffffffffffffffffffffffefffffffffffffffffffffffe";
    char bstr[1024] = "b4050a850c04b3abf54132565044b0b7d7bfd8ba270b39432355ffb4";
    char gxstr[1024] = "b70e0cbd6bb4bf7f321390b94a03c1d356c21122343280d6115c1d21";
    char gystr[1024] = "bd376388b5f723fb4c22dfe6cd4375a05a07476444d5819985007e34";
    char strn[1024] = "ffffffffffffffffffffffffffff16a2e0b8f03e13dd29455c5c2a3d";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;

}

struct curve get_p192() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "fffffffffffffffffffffffffffffffeffffffffffffffff";
    char astr[1024] = "fffffffffffffffffffffffffffffffefffffffffffffffc";
    char bstr[1024] = "64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1";
    char gxstr[1024] = "188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012";
    char gystr[1024] = "07192b95ffc8da78631011ed6b24cdd573f977a11e794811";
    char strn[1024] = "ffffffffffffffffffffffff99def836146bc9b1b4d22831";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}

struct curve get_p384() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000ffffffff";
    char astr[1024] = "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000fffffffc";
    char bstr[1024] = "b3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef";
    char gxstr[1024] = "aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7";
    char gystr[1024] = "3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f";
    char strn[1024] = "ffffffffffffffffffffffffffffffffffffffffffffffffc7634d81f4372ddf581a0db248b0a77aecec196accc52973";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}

struct curve get_p521() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "01ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";
    char astr[1024] = "01fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc";
    char bstr[1024] = "0051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b489918ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1ef451fd46b503f00";
    char gxstr[1024] = "00c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66";
    char gystr[1024] = "011839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650";
    char strn[1024] = "01fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa51868783bf2f966b7fcc0148f709a5d03bb5c9b8899c47aebb6fb71e91386409";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}

struct curve get_k163() {
    struct point g; struct curve c;
    mpz_init(c.p); mpz_init(c.a); mpz_init(c.b); mpz_init(c.n); mpz_init(g.x); mpz_init(g.y); c.g = g;
    char pstr[1024] = "01ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";
    char astr[1024] = "01fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc";
    char bstr[1024] = "0051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b489918ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1ef451fd46b503f00";
    char gxstr[1024] = "00c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66";
    char gystr[1024] = "011839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650";
    char strn[1024] = "01fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa51868783bf2f966b7fcc0148f709a5d03bb5c9b8899c47aebb6fb71e91386409";
    mpz_set_str(c.p,pstr,16); mpz_set_str(c.a,astr,16); mpz_set_str(c.b,bstr,16); mpz_set_str(c.g.x,gxstr,16); mpz_set_str(c.g.y,gystr,16); mpz_set_str(c.n,strn,16);
    return c;
}
