#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <ecc.h>
#include <string.h>

int main() {
    struct curve c = get_secp256k1();
    printf("Type 0 for random: ");
    char P[100]; scanf("%99s",P);
    mpz_t priv; mpz_init(priv);
    if (strcmp(P,"0") == 0) {
        printf("Generating random value...\n");
        struct key R; init_key(&R); make_key(&R,c);
        mpz_set(priv,R.r);
    } else {
    mpz_set_str(priv,P,16);
    }
    gmp_printf("Base 10: %Zd\n",priv);
    struct point pub; init_point(&pub); scalar_mult(&pub,priv,c.g,c);
    char * C = compress(pub);
    gmp_printf("X: %Zd\nY: %Zd\n",pub.x,pub.y); printf("Compressed: %s\n",C);
    struct point compcheck; init_point(&compcheck); decompress(&compcheck,C,c);
    gmp_printf("X: %Zd\nY: %Zd\n",compcheck.x,compcheck.y);
    mpz_t Z; mpz_init(Z); mpz_set_ui(Z,100);
    struct point sign; init_point(&sign); sign_message(&sign,Z,priv,c);
    int check = verify_signature(pub,Z,sign,c);
    gmp_printf("Signature: %Zd\n%Zd\nVerified: %d\n",sign.x,sign.y,check);
}
