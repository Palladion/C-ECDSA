#include <stdio.h>
#include <ecc.h>
#include <stdlib.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <string.h>

int main() {
    //initalize
    struct curve c = get_secp256k1();
    struct key keys; init_key(&keys); make_key(&keys,c);
    char *comp = compress(keys.p);
    //read file
    char filename[16]; printf("File: "); scanf("%14s",filename);
    FILE *file = fopen(filename,"r"); if (file == NULL) { printf("Failed to read file\n"); return 0; }
    int size; fseek(file,0L,SEEK_END); size = ftell(file); fseek(file,0L,SEEK_SET);
    unsigned char *filedata = malloc(size*sizeof(unsigned char));
    fread(filedata,size,1,file);
    //get key and do key exchange
    printf("Key: "); char recv[70]; scanf("%69s",recv); struct point them = decompress(recv,c);
    struct point shared; init_point(&shared); scalar_mult(&shared, keys.r,them,c);
    struct digest hash; init_digest(&hash); sha_point(&hash,shared);
    //get padding and copy to padded data
    int padding = size;
    int checking = 1; while (checking) {
        if (padding % 16 == 0) {
            checking = 0;
        } else {
            padding++;
        }
    }
    unsigned char *data = malloc(padding*sizeof(unsigned char));
    for (int x = 0; x < size; x++) { data[x] = filedata[x]; }
    //checksum stuff happen
    unsigned char checksum[SHA256_DIGEST_LENGTH];
    SHA256_CTX shaContext; SHA256_Init(&shaContext); SHA256_Update(&shaContext,data,padding); SHA256_Final(checksum,&shaContext);
    mpz_t Z; mpz_init(Z); char ze[65]; for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) { sprintf(&ze[x*2],"%02x",(unsigned int)checksum[x]); }
    mpz_set_str(Z,ze,16);
    struct point sign; init_point(&sign); sign_message(&sign,Z,keys.r,c);
    //AES key initalize and encrypt!
    AES_KEY enc_key; AES_set_encrypt_key(hash.digest,256,&enc_key);
    int z = 0; unsigned char buffer[17];
    for (int x = 0; x < padding/16; x++) {
        for (int y = 0; y < 16; y++) {
            buffer[y] = data[z+y];
        }
        AES_encrypt(buffer,buffer,&enc_key);
        for (int y = 0; y < 16; y++) {
            data[z+y] = buffer[y];
        } z = z + 16;
    SHA256_Init(&shaContext); SHA256_Update(&shaContext,hash.digest,32); SHA256_Final(hash.digest,&shaContext);
    AES_set_encrypt_key(hash.digest,256,&enc_key);
    } fclose(file);
    //write output
    FILE *output = fopen(filename,"w");
    char R[66]; gmp_sprintf(R,"%Zx",sign.x);
    for (int x = 0; x < 66-strlen(R); x++) {
        R[strlen(R)+x] = '|';
    }
    char S[66]; gmp_sprintf(S,"%Zx",sign.y);
    for (int x = 0; x < 66-strlen(S); x++) {
        S[strlen(S)+x] = '|';
    }
    fwrite(R,1,66,file); fwrite(S,1,66,file); fwrite(comp,1,66,file); fwrite(data,1,padding,file);
    fclose(output);
}
