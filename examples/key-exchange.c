//library
#include <ecc.h>
//GMP for big nums
#include <gmp.h>
//user input and output
#include <stdio.h>

int main() {
    struct curve c = get_secp256k1();
    mpz_t one; mpz_init(one); mpz_t two; mpz_init(two); struct key rand;
    printf("Enter One, type 0 for random: "); char private[1024]; scanf("%1023s",private); mpz_set_str(one,private,10);
    printf("Enter Two, type 0 for random: "); scanf("%1023s",private); mpz_set_str(two,private,10);
    if (mpz_cmp_ui(one,0) == 0) {
        init_key(&rand); make_key(&rand,c); mpz_set(one,rand.r);
    } if (mpz_cmp_ui(two,0) == 0) {
        init_key(&rand); make_key(&rand,c); mpz_set(two,rand.r);
    }
    printf("one: "); mpz_out_str(stdout,10,one); printf(" two: "); mpz_out_str(stdout,10,two); printf("\n");
    struct point O; init_point(&O); scalar_mult(&O,one,c.g,c); struct point T; init_point(&T); scalar_mult(&T,two,c.g,c);
    struct point Oshared; init_point(&Oshared); scalar_mult(&Oshared,one,T,c); struct point Tshared; init_point(&Tshared); scalar_mult(&Tshared,two,O,c);
    printf("ONE: "); mpz_out_str(stdout,16,O.x); printf(" TWO: "); mpz_out_str(stdout,16,T.y); printf("\n");
    printf("Shared O: "); mpz_out_str(stdout,16,Oshared.x); printf("\n");
    printf("Shared T: "); mpz_out_str(stdout,16,Tshared.x); printf("\n");
    struct digest hash; init_digest(&hash); sha_point(&hash,Oshared);
    printf("Shared SHA256 hash: %s\n",hash.hex);
    return 0;
}
