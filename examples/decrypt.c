#include <stdio.h>
#include <ecc.h>
#include <stdlib.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <string.h>

int main() {
    //initalize
    struct curve c = get_secp256k1();
    //read file
    char filename[16]; printf("File: "); scanf("%14s",filename);
    FILE *file = fopen(filename,"r"); if (file == NULL) { printf("Failed to read file\n"); return 0; }
    int size; fseek(file,0L,SEEK_END); size = ftell(file); fseek(file,0L,SEEK_SET); size = size - (65*3);
    char keycomp[67];
    unsigned char *filedata = malloc(size*sizeof(unsigned char));
    char R[67]; char S[67];
    fread(R,66,1,file); fread(S,66,1,file);
    fread(keycomp,66,1,file);
    fread(filedata,size,1,file);
    //get priv and do key exchange
    mpz_t P; mpz_init(P); char priv[100]; printf("Private: "); scanf("%99s",priv);
    struct point them = decompress(keycomp,c);
    mpz_set_str(P,priv,16); struct point shared; init_point(&shared); scalar_mult(&shared,P,them,c);
    struct digest hash; init_digest(&hash); sha_point(&hash,shared);
    //do decryption
    SHA256_CTX shaContext;
    AES_KEY dec_key; AES_set_decrypt_key(hash.digest,256,&dec_key);
    int z = 0; unsigned char buffer[17];
    for (int x = 0; x < size/16; x++) {
        for (int y = 0; y < 16; y++) { buffer[y] = filedata[z+y]; }
        AES_decrypt(buffer,buffer,&dec_key);
        for (int y = 0; y < 16; y++) { filedata[z+y] = buffer[y]; }
        z = z + 16;
    SHA256_Init(&shaContext); SHA256_Update(&shaContext,hash.digest,32); SHA256_Final(hash.digest,&shaContext);
    AES_set_decrypt_key(hash.digest,256,&dec_key);
    } fclose(file);
    //checksum check!
    unsigned char checksum[SHA256_DIGEST_LENGTH];
    SHA256_Init(&shaContext); SHA256_Update(&shaContext,filedata,size-3); SHA256_Final(checksum,&shaContext);
    mpz_t Z; mpz_init(Z); char ze[65]; for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) { sprintf(&ze[x*2],"%02x",(unsigned int)checksum[x]); }
    mpz_set_str(Z,ze,16);
    struct point sign; mpz_init(sign.x); mpz_init(sign.y);
    for (int x = 0; x < 66; x++) { if (R[x] == '|') { R[x] = 0; } if (S[x] == '|') { S[x] = 0; } }
    mpz_set_str(sign.x,R,16); mpz_set_str(sign.y,S,16);
    if (verify_signature(them,Z,sign,c)) {
        printf("Decrypted!\n");
        FILE *output = fopen(filename,"w"); fwrite(filedata,1,size,file); fclose(output);
    } else {
        printf("Failed checksum!\n");
    }
}
