#include <ecc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#define PORTNUM 2300

void xor(char * text, char * key) { for (int x = 0; x < 32; x++) { text[x] ^= key[x]; } }

void decrypt(unsigned char * text, char * k) {
    AES_KEY dec_key;
    unsigned char key[SHA256_DIGEST_LENGTH]; SHA256_CTX shaContext; SHA256_Init(&shaContext); SHA256_Update(&shaContext,k,strlen(k)); SHA256_Final(key,&shaContext);
    AES_set_decrypt_key(key,128,&dec_key);
    AES_decrypt(text,text,&dec_key);
}

int main() {
    struct curve c = get_secp256k1();
    struct key keys; init_key(&keys); make_key(&keys,c);
    char *comp = compress(keys.p);
    unsigned char buffer[17]; char IP[15]; printf("IP: "); scanf("%14s",IP);
    int len,mysocket;
    struct sockaddr_in dest;
    mysocket = socket(AF_INET,SOCK_STREAM,0);
    memset(&dest,0,sizeof(dest));
    dest.sin_family = AF_INET; dest.sin_addr.s_addr = inet_addr(IP); dest.sin_port = htons(PORTNUM);
    connect(mysocket,(struct sockaddr *)&dest,sizeof(struct sockaddr_in));
    char server_key[67];
    len = recv(mysocket,server_key,66,0); server_key[len] = '\0';
    struct point server; server = decompress(server_key,c);
    send(mysocket,comp,strlen(comp),0);
    struct point shared; init_point(&shared); scalar_mult(&shared,keys.r,server,c);
    //printf("[Debug] Server X: "); mpz_out_str(stdout,16,server.x); printf("\n");
    //printf("[Debug] Server Y: "); mpz_out_str(stdout,16,server.y); printf("\n");
    //printf("[Debug] Shared: "); mpz_out_str(stdout,16,shared.x); printf("\n");
    struct digest out; init_digest(&out); sha_point(&out,shared);
    recv(mysocket,buffer,16,0);
    //printf("[Debug] Encrypted text: "); for (int x = 0; x < 16; x++) { printf("%02x",buffer[x]); if (x != 15) { printf(":"); } } printf("\n");
    decrypt(buffer,out.hex);
    printf("Message: %s",buffer);
}
