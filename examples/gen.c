#include <stdio.h>
#include <stdlib.h>
#include <ecc.h>
#include <string.h>

int main() {
    struct curve c = get_secp256k1();
    int amount; struct key keys; char *comp;
    printf("Generate how many keys? "); if (scanf("%d",&amount) != 1) { printf("Error!\n"); return 0; }
    printf("Vanity? (write N for no vanity) "); int v = 1; char van[5]; scanf("%4s",van);
    if (van[0] == 'N' | van[0] == 'n') { printf("No vanity!\n"); v = 0; }
    if (v == 0) {
        for (int x = 0; x < amount; x++) {
            init_key(&keys); make_key(&keys,c);
            printf("%d ",x+1); mpz_out_str(stdout,16,keys.r);
            comp = compress(keys.p); printf(" %s\n",comp);
        }
    } else {
        int gend = 0;
        int check = 0;
        while (gend != amount) {
            init_key(&keys); make_key(&keys,c); comp = compress(keys.p);
            check = 1;
            for (int x = 0; x < strlen(van); x++) {
                if (van[x] != comp[x]) {
                    check = 0;
                }
            } if (check) {
                printf("%d ",gend+1); mpz_out_str(stdout,16,keys.r); printf(" %s\n",comp);
                gend++;
            }
        }
    }
}
