cc client.c -lgmp -lcrypto -l:ecc.a -o client
cc decrypt.c -lgmp -lcrypto -l:ecc.a -o decrypt
cc example.c -lgmp -lcrypto -l:ecc.a -o example
cc key-exchange.c -lgmp -lcrypto -l:ecc.a -o exchange
cc sign.c -lgmp -lcrypto -l:ecc.a -o sign
cc encrypt.c -lgmp -lcrypto -l:ecc.a -o encrypt
cc gen.c -lgmp -lcrypto -l:ecc.a -o gen
cc server.c -lgmp -lcrypto -l:ecc.a -o server
cc verify.c -lgmp -lcrypto -l:ecc.a -o verify



