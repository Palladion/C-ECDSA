#include <stdio.h>
#include <gmp.h>
//#include <openssl/md5.h>
#include <openssl/sha.h>
#include <ecc.h>
#include <string.h>

void hash_file(char * out) {
    char filename[50]; printf("File: "); scanf("%49s",filename);
    printf("Generating hash....");
    for (int x = 0; x < 32; x++) { out[x] = 0; }
    unsigned char c[SHA256_DIGEST_LENGTH];
    FILE *inFile = fopen(filename,"rb"); SHA256_CTX mdContext;
    int bytes; unsigned char data[1024];
    if (inFile == NULL) { printf("FAILED - File does not exist!\n"); out[0] = 0; return; }
    SHA256_Init(&mdContext); while ((bytes = fread(data,1,1024,inFile)) != 0) { SHA256_Update(&mdContext,data,bytes); }
    SHA256_Final(c,&mdContext);
    for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) {
        sprintf(&out[x*2], "%02x", (unsigned int)c[x]);
    }
}


/*void hash_file(char * out) {
    char filename[50]; printf("File: "); scanf("%49s",filename);
    printf("Generating hash....");
    for (int x = 0; x < 32; x++) { out[x] = 0; }
    unsigned char c[MD5_DIGEST_LENGTH];
    FILE *inFile = fopen(filename,"rb"); MD5_CTX mdContext;
    int bytes; unsigned char data[1024];
    if (inFile == NULL) { printf("FAILED - File does not exist\n"); out[0] = 0; return; }
    MD5_Init(&mdContext); while ((bytes = fread(data,1,1024,inFile)) != 0) { MD5_Update(&mdContext,data,bytes); }
    MD5_Final(c,&mdContext);
    for (int x = 0; x < MD5_DIGEST_LENGTH; x++) {
        sprintf(&out[x*2], "%02x", (unsigned int)c[x]);
    }
}*/

int main() {
    char out[100]; hash_file(out); if (out[0] == 0) { return 0; } printf("%s\n",out);
    char verify[50]; printf("Signature: "); scanf("%49s",verify);
    struct curve c = get_secp256k1();
    char X[100]; char P[100]; char R[100]; char S[100]; FILE *signFile = fopen(verify,"r");
    if (signFile == NULL) { return 0; }
    fgets(X,99,signFile); fgets(R,99,signFile); fgets(S,99,signFile);
    struct point public = decompress(X,c);
    struct point sign; mpz_init(sign.x); mpz_init(sign.y);
    mpz_set_str(sign.x,R,16); mpz_set_str(sign.y,S,16);
    mpz_t digest; mpz_init(digest); mpz_set_str(digest,out,16);
    if (verify_signature(public,digest,sign,c)) {
        printf("\033[1;32mSignature Good!\033[0m\n");
        char *comp = compress(public);
        printf("Signed with key: %s\n",comp);
    } else {
        printf("\033[1;31mBAD SIGNATURE!\033[0m\n");
    }
}
