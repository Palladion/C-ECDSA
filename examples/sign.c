#include <stdio.h>
#include <gmp.h>
//#include <openssl/md5.h>
#include <openssl/sha.h>
#include <ecc.h>
#include <unistd.h>


void hash_file(char * out) {
    char filename[50]; printf("File: "); scanf("%49s",filename);
    printf("Generating hash....");
    for (int x = 0; x < 32; x++) { out[x] = 0; }
    unsigned char c[SHA256_DIGEST_LENGTH];
    FILE *inFile = fopen(filename,"rb"); SHA256_CTX mdContext;
    int bytes; unsigned char data[1024];
    if (inFile == NULL) { printf("FAILED - File does not exist!\n"); out[0] = 0; return; }
    SHA256_Init(&mdContext); while ((bytes = fread(data,1,1024,inFile)) != 0) { SHA256_Update(&mdContext,data,bytes); }
    SHA256_Final(c,&mdContext);
    for (int x = 0; x < SHA256_DIGEST_LENGTH; x++) {
        sprintf(&out[x*2], "%02x", (unsigned int)c[x]);
    }
}

/*
void hash_file(char * out) {
    char filename[50]; printf("File: "); scanf("%49s",filename);
    printf("Generating hash....");
    for (int x = 0; x < 32; x++) { out[x] = 0; }
    unsigned char c[MD5_DIGEST_LENGTH];
    FILE *inFile = fopen(filename,"rb"); MD5_CTX mdContext;
    int bytes; unsigned char data[1024];
    if (inFile == NULL) { printf("FAILED - File does not exist!\n"); out[0] = 0; return; }
    MD5_Init(&mdContext); while ((bytes = fread(data,1,1024,inFile)) != 0) { MD5_Update(&mdContext,data,bytes); }
    MD5_Final(c,&mdContext);
    for (int x = 0; x < MD5_DIGEST_LENGTH; x++) {
        sprintf(&out[x*2], "%02x", (unsigned int)c[x]);
    }
}*/


int main() {
    char out[100]; hash_file(out); if (out[0] == 0) { return 0; } printf("%s\n",out);
    //char *p = getpass("Private: "); //You COULD use this, but here it outputs on console
    printf("Private: "); char p[1024]; scanf("%1023s",p);
    mpz_t priv; mpz_init(priv); mpz_set_str(priv,p,16);
    struct curve c = get_secp256k1();
    struct point pub; init_point(&pub); scalar_mult(&pub,priv,c.g,c);
    char *comp = compress(pub);
    printf("Public: %s\n",comp);
    mpz_t digest; mpz_init(digest); mpz_set_str(digest,out,16);
    printf("Sign? (Y/N) "); char uwu[2]; scanf("%1s",uwu); if (uwu[0] != 'y') { return 0; }
    struct point sign; init_point(&sign); sign_message(&sign,digest,priv,c);
    if (verify_signature(pub,digest,sign,c)) {
        printf("Signature Generated!\n");
        FILE *outFile = fopen("signature","w+");
        char R[100]; gmp_sprintf(R,"%Zx",sign.x);
        char S[100]; gmp_sprintf(S,"%Zx",sign.y);
        fprintf(outFile,"%s\n%s\n%s\n",comp,R,S);
    }
}
