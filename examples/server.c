#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <ecc.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#define PORTNUM 2300

void xor(char * text, char * key) { for (int x = 0; x < 32; x++) { text[x] ^= key[x];  }  }

void encrypt(unsigned char * text, char * k) {
    AES_KEY enc_key;
    unsigned char key[SHA256_DIGEST_LENGTH]; SHA256_CTX shaContext; SHA256_Init(&shaContext); SHA256_Update(&shaContext,k,strlen(k)); SHA256_Final(key,&shaContext);
    AES_set_encrypt_key(key,128,&enc_key);
    AES_encrypt(text,text,&enc_key);
}

void decrypt(unsigned char * text, char * k) {
    AES_KEY dec_key;
    unsigned char key[SHA256_DIGEST_LENGTH]; SHA256_CTX shaContext; SHA256_Init(&shaContext); SHA256_Update(&shaContext,k,strlen(k)); SHA256_Final(key,&shaContext);
    AES_set_decrypt_key(key,128,&dec_key);
    AES_decrypt(text,text,&dec_key);
}


int main() {
    printf("Running...\n");
    struct curve c = get_secp256k1();
    struct key keys; init_key(&keys); make_key(&keys,c);
    char* comp = compress(keys.p);
    unsigned char message[16]; printf("Message to echo: "); fgets(message,16,stdin); //scanf("%31s",message);
    char X[100]; gmp_sprintf(X,"%Zx",keys.p.x);
    char Y[100]; gmp_sprintf(Y,"%Zx",keys.p.y);
    struct sockaddr_in dest; struct sockaddr_in serv;
    int len,mysocket; socklen_t socksize = sizeof(struct sockaddr_in);
    memset(&serv,0,sizeof(serv));
    serv.sin_family = AF_INET; serv.sin_addr.s_addr = htonl(INADDR_ANY); serv.sin_port = htons(PORTNUM);
    mysocket = socket(AF_INET,SOCK_STREAM,0);
    bind(mysocket,(struct sockaddr *)&serv,sizeof(struct sockaddr));
    listen(mysocket,1);
    int consocket = 1;
    printf("[Server] Server started!\n");
    printf("[Server] Server Key: "); mpz_out_str(stdout,16,keys.p.x); printf("\n");
    while (consocket) {
        char client_key[67];
        consocket = accept(mysocket,(struct sockaddr *)&dest, &socksize);
        send(consocket,comp,strlen(comp),0);
        struct point client;
        len = recv(consocket,client_key,66,0); client_key[len] = '\0';
        client = decompress(client_key,c);
        struct point shared; init_point(&shared); scalar_mult(&shared,keys.r,client,c);
        //printf("[Debug] Client X: "); mpz_out_str(stdout,16,client.x); printf("\n");
        //printf("[Debug] Client Y: "); mpz_out_str(stdout,16,client.y); printf("\n");
        //printf("[Debug] Shared: "); mpz_out_str(stdout,16,shared.x); printf("\n");
        struct digest out; init_digest(&out); sha_point(&out,shared);
        encrypt(message,out.hex); send(consocket,message,32,0); decrypt(message,out.hex);
        close(consocket);
    }
}
